package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    Random random = new Random();

    String humanChoice = "";
    String computerChoice = "";
    String choiceString = "";

    int randomInt;
    int roundCounter = 1;
    int humanScore;
    int computerScore;

    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    

    public void makeHumanChoice() {
        while (true){
            humanChoice = "";
            String humanChoiceUpper = readInput("Your choice (Rock/Paper/Scissors)? ");
            humanChoice = humanChoiceUpper.toLowerCase();
            if (rpsChoices.contains(humanChoice)) {
                break;
            }
            else {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            }
        }
    }

    public void makeComputerChoice() {
        randomInt = random.nextInt(rpsChoices.size());
        computerChoice = rpsChoices.get(randomInt);
    }

    static boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }
    }

    public void changeCorrectScore() {
        if (isWinner(humanChoice, computerChoice)) {
            System.out.println(choiceString + " Human wins!");
            humanScore ++;
        }
        else if (isWinner(computerChoice, humanChoice)) {
            System.out.println(choiceString + " Computer wins!");
            computerScore ++;
        }
        else {
            System.out.println(choiceString + " It's a tie.");
        }
    }

    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            makeHumanChoice();
            makeComputerChoice();
            choiceString = "Human chose " + humanChoice + ", computer chose " + computerChoice + ".";
            changeCorrectScore();
            System.out.println("Score: human " + humanScore + " computer " + computerScore);

            String continueChoice = readInput("Do you wish to continue playing? (y/n)?");
            if (continueChoice.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter ++;
        }    
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
